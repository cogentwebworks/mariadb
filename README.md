## small Mariadb 10.x container based on latest Alpine with workings6-overlay process and socklog-overlay.

[![Docker Pulls](https://img.shields.io/docker/pulls/cogentwebs/mariadb.svg)](https://hub.docker.com/r/cogentwebs/mariadb/)
[![Docker Stars](https://img.shields.io/docker/stars/cogentwebs/mariadb.svg)](https://hub.docker.com/r/cogentwebs/mariadb/)
[![Docker Build](https://img.shields.io/docker/automated/cogentwebs/mariadb.svg)](https://hub.docker.com/r/cogentwebs/mariadb/)
[![Docker Build Status](https://img.shields.io/docker/build/cogentwebs/mariadb.svg)](https://hub.docker.com/r/cogentwebs/mariadb/)

This is a small mariadb container but still have a working s6-overlay process and  socklog-overlay . This is the base image for general small containers.
